import {applyMiddleware, compose, createStore} from "redux";

import thunk from "redux-thunk";
import reducersRoot from "./redux/reducers";

function saveToLocalStorage(state) {
    try {
        const serializedState = JSON.stringify(state);
        // console.log("saving serialized state:", serializedState)
        localStorage.setItem("state", serializedState);
        // console.log("saved state to local storage", localStorage.getItem('state'))
    } catch (e) {
        console.log(e);
    }
}

const reHydrateStore = () => {
    // <-- FOCUS HERE

    if (localStorage.getItem("state") !== null) {
        return JSON.parse(localStorage.getItem("state")); // re-hydrate the store
    }
};

function loadFromLocalStorage() {
    try {
        const serializedState = localStorage.getItem("state");
        // console.log("serializedState: ", serializedState)
        if (serializedState === null) return undefined;
        return JSON.parse(serializedState);
    } catch (err) {
        console.log(err);
        return undefined;
    }
}

const persistedState = loadFromLocalStorage();
const refreshState = reHydrateStore();

let store = createStore(
    reducersRoot,

    persistedState,
    compose(
        applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : (f) => f
    )
);

store.subscribe(() => saveToLocalStorage(store.getState()));
export default store;
