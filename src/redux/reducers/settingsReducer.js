import {SET_AVATAR_IMAGE, SET_COLOR_1, SET_COLOR_2, SET_FORM_DATA} from "../actions/settingsActions";

const initialState = {
    name:"",
    description:"",
    jobTitle:"",
    jobLevel:"",
    phoneNumber:"",
    address:"",
    color1:"#7e57c2",
    color2:"#5c6bc0",
    avatarImage:null
};
export default function settingsReducer(state = initialState, action) {
    switch (action.type) {
        case SET_COLOR_1:
            return {
                ...state,
                color1: action.data,
            };
        case SET_COLOR_2:
            return {
                ...state,
                color2: action.data,
            };
        case SET_FORM_DATA:
            return {
                ...state,
                ...action.data,
            };
        case SET_AVATAR_IMAGE:
            return {
                ...state,
                avatarImage: action.data,
            };
        default:
            return state;
    }
}
