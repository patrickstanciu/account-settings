import React, {useState} from "react";
import "./AccountSettings.scss";
import Aside from "../../components/Aside/Aside";
import Preview from "../../components/Preview/Preview";

function AccountSettings(props) {
    const [showingAlert, setShowingAlert] = useState(false)

    return (
        <div className='wrapper' id="account-settings">
            <div className="sidebar">
                <Aside setShowingAlert={setShowingAlert}/>
            </div>
            <div className='panel-main'>
                <Preview showingAlert={showingAlert} setShowingAlert={setShowingAlert} />
            </div>
        </div>
    );
}

export default AccountSettings;
