import {SET_AVATAR_IMAGE, SET_COLOR_1, SET_COLOR_2, SET_FORM_DATA} from "./settingsActions";

export const setColor1 = (data) => {
    return {
        type: SET_COLOR_1,
        data,
    };
};

export const setColor2 = (data) => {
    return {
        type: SET_COLOR_2,
        data,
    };
};

export const setAvatarImage = (data) => {
    return {
        type: SET_AVATAR_IMAGE,
        data,
    };
};
export const setAccountDetails = (data) => {
    return {
        type: SET_FORM_DATA,
        data,
    };
};