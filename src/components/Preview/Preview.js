import React, {useState} from "react";
import "./Preview.scss";
import Header from "../Header/Header";
import profilePlaceholder from "../../static/images/profile-placeholder.png";
import {useSelector} from "react-redux";

function Preview(props) {
    const avatarImage = useSelector((state) => state.settings.avatarImage) || profilePlaceholder;
    const jobLevel = useSelector((state) => state.settings.jobLevel) || "";
    const jobTitle = useSelector((state) => state.settings.jobTitle) || "";
    const phoneNumber = useSelector((state) => state.settings.phoneNumber) || "";
    const address = useSelector((state) => state.settings.address) || "";

    const {showingAlert,setShowingAlert} = props

    return (
        <div id="preview">
            <Header/>
            <div className={"user-description"}>
                <div className={"row"}>
                    <div className={"col-5"}>
                        <img style={{padding: "15px", width: "100%", objectFit: "contain"}} src={avatarImage}/>
                    </div>
                    <div className={"col description-labels"}>
                        <p className={"data-labels"}>Current Job</p>
                        <p className={"data-text"}>{jobTitle}</p>
                        <p className={"data-labels"}>Level</p>
                        <p className={"data-text"}>{jobLevel}</p>
                        <p className={"data-labels"}>Phone Number</p>
                        <p className={"data-text"}>{phoneNumber}</p>
                        <p className={"data-labels"}>Address</p>
                        <p className={"data-text"}>{address}</p>
                    </div>
                </div>
            </div>

            <div className={"alert-container"}>
                {/*<div className={"alert-child"}>
                    qddqdq
                </div>*/}
               {/* <div className="alert alert-info alert-child" role="alert">
                    Line 2
                    <a className="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>*/}
                <div>
                    <div className={`alert alert-success alert-child ${showingAlert ? 'alert-shown' : 'alert-hidden'}`}>
                        One or more fields are empty
                        <a onClick={()=>setShowingAlert(!showingAlert)} style={{cursor:"pointer"}} className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Preview;
