import React from "react";
import "./App.css";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import store from "./store";
import {Provider} from "react-redux";
import AccountSettings from "./containers/AccountSettings/AccountSettings";

export default () => (
    <Provider store={store}>
        <Router>
            <Switch>
                <Route
                    path="/"
                    exact
                    component={AccountSettings}
                />
            </Switch>
        </Router>
    </Provider>
);
