import React from "react";
import "./Header.scss";
import profilePlaceholder from "../../static/images/profile-placeholder.png";
import { useSelector} from "react-redux";
import Draggable from 'react-draggable';

function Header(props) {

    const color1 = useSelector((state) => state.settings.color1) || "";
    const color2 = useSelector((state) => state.settings.color2) || "";
    const name = useSelector((state) => state.settings.name) || "";
    const description = useSelector((state) => state.settings.description) || "";
    const avatarImage = useSelector((state) => state.settings.avatarImage) || profilePlaceholder;

    function myFunction(elem) {
        var div = document?.getElementById(elem);
        var rect = div?.getBoundingClientRect();
        console.log(rect?.left)

        return rect?.left;
    }
    /*const [color1header, setColor1header] = useState(color1); // set campaign as default
    const [color2header, setColor2header] = useState(color2); // set campaign as default

    useEffect(() => {
        setColor1header(color1)
    }, [color1]);
    useEffect(() => {
        setColor2header(color2)
    }, [color2]);*/



    return (
        <div id="header"  style={{ backgroundImage: `linear-gradient(to right, ${color1} , ${color2})`}}>

            <div className={"row"}>
                <div className={"col-2"} >
                    <Draggable bounds={{top: -15, left: -10, right: 700, bottom: 40}} >
                    <img style={{ padding:"15px", width: "100%", objectFit:"contain"}} src={avatarImage}/>
                    </Draggable>
                </div>
                <div className={"col"}>
                    <Draggable bounds={{top: -15, left: -150, right: 550, bottom: 60}} >
                    <p className={"title"}>{name}</p>
                    </Draggable>
                    <Draggable bounds={{top: -70, left: -150, right: 550, bottom: 20}} >
                    <p className={"default"}>{description}</p>
                    </Draggable>
                </div>
            </div>
        </div>
    );
}

export default Header;
