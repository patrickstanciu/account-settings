import React from "react";
import "./ButtonRectangle.scss";

function ButtonRectangle(props) {
    let btnClasses = "but " + (props.otherClass ? props.otherClass : "");

    if (props.icon || props.leadingIcon) {
        btnClasses += " big";
    }

    return (
        <button
            id="button-rectangle"
            style={props.style}
            disabled={props.disabled}
            type="button"
            className={btnClasses}
            onClick={props.onClick}
        >
            {props.text}
            {props.loading && <span style={{marginLeft: "10px", marginRight: "-26px"}}
                                    className="pl-2 mb-1 spinner-border spinner-border-sm"/>}
        </button>
    );
}

export default ButtonRectangle;
