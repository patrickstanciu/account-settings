export const SET_COLOR_1 = "SET_COLOR_1";
export const SET_COLOR_2 = "SET_COLOR_2";
export const SET_AVATAR_IMAGE = "SET_AVATAR_IMAGE";
export const SET_FORM_DATA = "SET_FORM_DATA";
