import React, {useState} from "react";
import "./Aside.scss";
import ColorPicker from "../ColorPicker/ColorPicker";
import 'react-dropdown/style.css';
import Select from "react-select";
import profilePlaceholder from "../../static/images/profile-placeholder.png";
import ButtonRectangle from "../ButtonRectangle/ButtonRectangle";
import {useDispatch, useSelector} from "react-redux";
import {setAccountDetails, setAvatarImage} from "../../redux/actions";

function Aside(props) {
    const dispatch = useDispatch();
    const avatarImage = useSelector((state) => state.settings.avatarImage) || profilePlaceholder;
    const color1 = useSelector((state) => state.settings.color1) || "";
    const color2 = useSelector((state) => state.settings.color2) || "";
    const [file, setFile] = useState("");
    const [avatarSelected, setAvatarSelected] = useState(null);
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [jobTitle, setJobTitle] = useState("");
    const [jobLevel, setJobLevel] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [address, setAddress] = useState("");
    const {showingAlert,setShowingAlert} = props

    function hexToRGB(hex, alpha) {
        const r = parseInt(hex.slice(1, 3), 16);
        const g = parseInt(hex.slice(3, 5), 16);
        const b = parseInt(hex.slice(5, 7), 16);
        if (alpha) {
            return {r: `${r}`, g: `${g}`, b: `${b}`, a: `${alpha}`}
        } else {
            return {r: `${r}`, g: `${g}`, b: `${b}`}
        }
    }

    const defaultSelectStyles = {
        control: base => ({
            ...base,
            border: 0,
            // This line disable the blue border
            boxShadow: 'none'
        }),
        indicatorSeparator: base => ({
            ...base,
            display: "none"
        }),
    };
    const changeAvatar = () => {
        document.getElementById("file-change-avatar").click();
    };

    function handleShowAlert() {
        setShowingAlert(true)
        setTimeout(() => {
            setShowingAlert(false)
        }, 3000);
    }
    const saveDetails = () => {
        if(!name || !description || !phoneNumber || !address || !jobTitle || !jobLevel) {
            handleShowAlert()
        }
         else {
            dispatch(setAccountDetails({
                name: name,
                description: description,
                phoneNumber: phoneNumber,
                address: address,
                jobLevel: jobLevel,
                jobTitle: jobTitle
            }))
        }
    };
    const onSelectedAvatarNewMemberChange = () => {
        const file = document.getElementById("file-change-avatar").files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
            setFile(file);
            setAvatarSelected(reader.result);
            dispatch(setAvatarImage(reader.result))

        };
        setAvatarSelected(reader.result);
    };
    const jobTitleValues = [
        {value: 'Fullstack web developer', label: 'Fullstack web developer', id: 0},
        {value: 'Frontend developer', label: 'Frontend developer', id: 1},
        {value: 'Backend developer', label: 'Backend developer', id: 2},
        {value: 'Mobile app developer', label: 'Mobile app developer', id: 3},
        {value: 'Devops Engineer', label: 'Devops Engineer', id: 4},
        {value: 'Software developer', label: 'Software developer', id: 5},
    ]
    const jobLevels = [
        {value: 'Junior 1', label: 'Junior 1', id: 0},
        {value: 'Junior 2', label: 'Junior 2', id: 1},
        {value: 'Junior 3', label: 'Junior 3', id: 2},
        {value: 'Middle 1', label: 'Middle 1', id: 3},
        {value: 'Middle 2', label: 'Middle 2', id: 4},
        {value: 'Middle 3', label: 'Middle 3', id: 5},
        {value: 'Senior 1', label: 'Senior 1', id: 6},
        {value: 'Senior 2', label: 'Senior 2', id: 7},
    ]
    return (
        <div id="aside">
            <p className={"title"}>Account Setup</p>
            <div className="details-wrapper">
                <div className="rectangle">
                    <div className="row container mb-2">
                        <img alt="profile-image" style={{width: "100px", height: "100px"}}
                             src={avatarSelected ? avatarSelected : avatarImage}/>
                        <div className="col">
                            <p className="default mb-0">Profile image</p>
                            <p className="notes">(JPG, PNG or GIF)</p>
                            <ButtonRectangle
                                otherClass={"upload-button"}
                                onClick={changeAvatar}
                                text="UPLOAD"
                                style={{marginTop: "12px", backgroundColor: "#32a8cd"}}/>
                            <input
                                type="file"
                                id="file-change-avatar"
                                accept="image/png, image/jpeg, image/gif"
                                onChange={onSelectedAvatarNewMemberChange}
                                style={{padding: "10px", display: "none", cursor: "pointer"}}
                            />

                        </div>
                    </div>
                    <p className="default">Header color</p>
                    <div className="row container">
                        <div className="row col container">
                            <p className="default" style={{marginRight: "20px"}}>Color 1</p>
                            <ColorPicker colorId={1} color={hexToRGB(color1, 1)} marginLeft={"-50px"}/>
                        </div>
                        <div className="row col container">
                            <p className="default" style={{marginRight: "20px"}}>Color 2</p>
                            <ColorPicker colorId={2} color={hexToRGB(color2, 1)} marginLeft={"-200px"}/>
                        </div>
                    </div>
                </div>
                <div className="rectangle">
                    <p className="default">Name</p>
                    <input
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        style={{width: "100%"}}
                    />
                    <p className="default" style={{marginTop: "10px"}}>Description</p>
                    <textarea
                        maxLength="250"
                        style={{width: "100%", height: "100px"}}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                    <p className="notes">max 250 characters</p>
                </div>
                <div className="rectangle">
                    <p className="default">Job title</p>
                    <Select
                        onChange={(e) => setJobTitle(e.value)}
                        value={
                            jobTitleValues.find((obj) => obj.value === jobTitle) || null
                        }
                        defaultValue={jobTitleValues[0]}
                        options={jobTitleValues}
                        styles={defaultSelectStyles}
                    />
                    <p className="default" style={{marginTop: "10px"}}>Job level</p>
                    <Select
                        onChange={(e) => setJobLevel(e.value)}
                        value={
                            jobLevels.find((obj) => obj.value === jobLevel) || null
                        }
                        defaultValue={jobLevels[0]}
                        options={jobLevels}
                        styles={defaultSelectStyles}
                    />
                </div>
                <div className="rectangle">
                    <p className="default">Phone Number</p>
                    <input
                        style={{width: "100%"}}
                        value={phoneNumber}
                        onChange={(e) => setPhoneNumber(e.target.value)}
                    />
                    <p className="default" style={{marginTop: "20px"}}>Address</p>
                    <textarea
                        style={{width: "100%", height: "100px"}}
                        value={address}
                        maxLength="250"
                        onChange={(e) => setAddress(e.target.value)}
                    />
                    <p className="notes">max 250 characters</p>
                </div>
            </div>

            <ButtonRectangle
                otherClass={"save-button"}
                style={{
                    margin: "20px", width: "150px",
                    backgroundColor: "#32a436", color: "white"
                }}
                text={"SAVE"}
                onClick={saveDetails}
            />
        </div>
    );
}

export default Aside;
