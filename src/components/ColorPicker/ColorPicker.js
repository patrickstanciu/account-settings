import React, {useState} from "react";
import reactCSS from 'reactcss'
import {SwatchesPicker} from 'react-color'
import {useDispatch} from "react-redux";
import {setColor1,setColor2} from "../../redux/actions";
function ColorPicker(props) {
    const dispatch = useDispatch();
    const [displayColorPicker, setDisplayColorPicker] = useState(false);
    const colors = [["#ef5350"], ["#ec407a"], ["#ab47bc"], ["#7e57c2"],["#5c6bc0"],
        ["#29b6f6"],["#26c6da"], ["#26a69a"], ["#66bb6a"], ["#9ccc65"], ["#ffee58"], ["#ffca28"],
        ["#f6c277"], ["#ffa726"], ["#ff7043"], ["#000000"], ["#616161"], ["#9e9e9e"], ["#e0e0e0"],
        ['#FFFFFF']
    ]
    const [color, setColor] = useState(props.color)

    const styles = reactCSS({
        'default': {
            color: {
                width: '25px',
                height: '25px',
                borderRadius: '5px',
                background: `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`,
            },
            swatch: {
                padding: '1px',
                background: '#fff',
                borderRadius: '5px',
                boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                display: 'inline-block',
                cursor: 'pointer',
            },
            popover: {
                marginLeft:props.marginLeft,
                position: 'absolute',
                zIndex: '2',
            },
            cover: {
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
            },
        },
    });
    const changeColor = (color) => {
        setColor(color.rgb);
        if(props.colorId===1) {
            dispatch(setColor1(color.hex))
        }
        else {
            dispatch(setColor2(color.hex))
        }
    };

    return (
        <div id={"color-picker"} style={{paddingBottom: "250px", marginBottom: "-250px"}}>
            <div style={styles.swatch} onClick={() => setDisplayColorPicker(!displayColorPicker)}>
                <div style={styles.color}/>
            </div>
            {displayColorPicker ? <div style={styles.popover}>
                <div style={styles.cover} onClick={() => setDisplayColorPicker(false)}/>
                <SwatchesPicker styles={"marginLeft:-200px"}  width={"300px"} colors={colors} color={color} onChange={(color)=>changeColor(color)}/>
            </div> : null}
        </div>
    );
}

export default ColorPicker